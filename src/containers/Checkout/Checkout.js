import React, {Component, Fragment} from 'react';
import {Route} from "react-router-dom";
import {connect} from "react-redux";

import CheckoutSummary from "../../components/Order/CheckoutSummary/CheckoutSummary";
import ContactData from "./ContactData/ContactData";

class Checkout extends Component {

  checkoutCancelledHandler = () => {
    this.props.history.goBack();
  };

  checkoutContinuedHandler = () => {
    this.props.history.replace('/checkout/contact-data');
  };

  render() {
    return (
      <Fragment>
        <CheckoutSummary
          ingredients={this.props.ingredients}
          checkoutContinued={this.checkoutContinuedHandler}
          checkoutCancelled={this.checkoutCancelledHandler}
        />
        <Route path={this.props.match.path + '/contact-data'} component={ContactData} />
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.bb.ingredients
  }
};

export default connect(mapStateToProps)(Checkout);