import React from 'react';
import './Logo.css';
import burgerLogo from '../../../assets/images/burger-logo.jpg';


const Logo = () => {
  return(
    <div className="Logo">
      <img src={burgerLogo} alt="MyBurger-logo"/>
    </div>
  )
};

export default Logo;