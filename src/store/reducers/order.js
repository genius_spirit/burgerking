import * as actionType from "../actions/actionTypes";

const initialState = {
  loading: false,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ORDER_REQUEST:
      return {...state, loading: true};
    case actionType.ORDER_SUCCESS:
      return {...state,loading: false};
    case actionType.ORDER_ERROR:
      return {...state, loading: false, error: action.error};
    default:
      return state;
  }
};

export default reducer;