import * as actionType from "./actionTypes";


export const orderRequest = () => {
  return {type: actionType.ORDER_REQUEST};
};

export const orderSuccess = () => {
  return {type: actionType.ORDER_SUCCESS};
};

export const  orderError = (error) => {
  return {type: actionType.ORDER_ERROR, error};
};